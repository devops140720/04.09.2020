
# statistics(k, *args, **kwargs)
# print the value ok f and its type (hint: 'positional', print(type(k)), print(k))
# print if *args was sent (hint use len or count or None...)
# print if **kwayrgs was sent (hint use len or count or None...)
# if *args was sent print: its length, print all of its values, print if it has repitition
# **kwargs:
# print length of the dictionary
# print all keys
# print all values
# print all items together (hint: for kv, in kwargs ...)
#  return- does k appear in dict (as key)?
# *etgar - return a list from kwargs: list1 - keys, list2 - values -- function will return 3 values
def statistics(k, *args, **kwargs):
    print(f'positional arguments: k = {k} type = {type(k)}')
    if len(args) == 0:
        print('*args argument: was not set so we have an empty tuple')
    else:
        print('*args:')
        print(f'number of items sent = {len(args)}')
        print(f'items = {args}')
        len_args = len(args)
        len_args_no_duplications = len(set(args))
        if len_args == len_args_no_duplications:
            print('no duplicated items in *args')
        else:
            print('there are duplications in *args')

    if len(kwargs) == 0:
        print('**kwargs argument: items were not set so we have an empty dictionary')
        return False, None, None
    else:
        print(f'length of kwargs (number of key-value items): {len(kwargs)}')
        counter = 0
        print('keys:')
        for key1 in kwargs.keys():
            counter = counter + 1
            print(f'[{counter}] : {key1}')
        counter = 0
        print('values:')
        for value1 in kwargs.values():
            counter = counter + 1
            print(f'[{counter}] : {value1}')
        print('key values:')
        counter = 0
        for k1, v1 in kwargs.items():
            counter=counter+1
            print(f'[{counter}] : key={k1} value={v1}')
        if k in kwargs.keys():
            return True, kwargs.keys(), kwargs.values()
        else:
            return False, kwargs.keys(), kwargs.values()

# def statistics(k, *args, **kwargs):
# def statistics(1, (1,1,7), { 'one':10, 'two':20 }):
my_key = 'one1'
k_in_dict, list_keys_kwargs, list_values_kwargs = statistics(my_key, 1, 1, 7, one=10, two=20);
print(f'was {my_key} in dictionary? {k_in_dict}')
print(list_keys_kwargs)
print(list_values_kwargs)