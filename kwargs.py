def sum1(l1, d1, t1, *args):
    result = 0
    for n in args:
        result = result + n
    return result

my_sum = sum1([1,2,3],{ 'o' : 1 } , (1,2,3))
print(my_sum)

for n in range(1,100000, 2):
    print(n)

#while range(1,100) can give more numbers
    #n = range(1,100) -- give me the next number
    #...

def my_add(x, y):
    return x + y
my_add(x=10, y=20)

# **kwargs
# 1 parameter which gets a dictionary
# 2 the key:value of the dicgtionary are the parameter sent to the function
# 3 the dictionary values are optioanl -- you can send or ignore
# like *args - accepts dictionary
# x -- the type you sent to the function
# *args -- tuple
# **kwargs -- dictionary
def printDictionary(x, *args, **kwargs):
    for k, v in kwargs.items():
        print(f'key: {k} value: {v}')

#printDictionary({ 'name' : 'danny' , 'age':12, 'city' : 'tv'})

printDictionary([1,2,3], 'a',[1,23], -20, key1=1)

printDictionary(1, name='danny',
                age=12,
                city='TV')




# statistics(k, *args, **kwargs)
# print the value ok f and its type (hint: 'positional', print(type(k)), print(k))
# print if *args was sent (hint use len or count or None...)
# print if **kwayrgs was sent (hint use len or count or None...)
# if *args was sent print: its length, print all of its values, print if it has repitition
# **kwargs:
# print length of the dictionary
# print all keys
# print all values
# print all items together (hint: for kv, in kwargs ...)
#  return- does k appear in dict (as key)?
# *etgar - return a list from kwargs: list1 - keys, list2 - values -- function will return 3 values
